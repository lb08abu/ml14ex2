#!/bin/env python3

import numpy as np


def f(z):
    # return an array of relevant sigmoid values
    if z < -100:
        return 0
    elif z > 100:
        return 1
    return 1 / (1 + np.exp(-z))


def sigmoid(data):
    if type(data) == int:
        data = np.array(data)
    return np.array([f(d) for d in data.flat]).reshape(data.shape)