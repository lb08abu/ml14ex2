#!/bin/env python3

import numpy as np
from plotData import plot_data2
from mapFeature import map_features
from costFunctionReg import compute_cost
from the_gaylord_code import gradient_descent
from plot_gradient_descent import plot_results
import scipy.optimize as opt


data = np.loadtxt("ex2data2.txt", delimiter=',')
Y = data[:, 2]
m = len(Y)
Y = Y.reshape((m, 1))

X1 = data[:, 0].reshape((m, 1))
X2 = data[:, 1].reshape((m, 1))

print(X1.shape)
print(X2.shape)
print(Y.shape)

# plot the data using plot_data2
# plot_data2(np.column_stack((np.ones((m, 1)), X1, X2)), Y)

# initialize
X = map_features(X1, X2, 6)
m, n = X.shape
theta = np.zeros((n, 1))
lamb = np.array([[1]])

J, grad = compute_cost(theta, X, Y, lamb)

print(J)
print(grad[1])
# print(X[:, 27])

if 1:
    # gradient descent
    num_iters = 2
    alpha = 0.03

    J_history = gradient_descent(theta, X, Y, num_iters, alpha, 100)

    print(J_history.shape)
    print(J_history[-1, :])

    # plot_results(J_history)


meth = 'L-BFGS-B', 'TNC', 'BFGS', 'CG', 'Newton-CG'

if 0:
    print("Optimize:")
    optimum = opt.minimize(
        compute_cost, theta.flatten(),
        method=meth[2],
        jac=True, args=(X, Y.flatten(), lamb.flatten()),
        options={'maxiter': 400})

    print("fun:     %s" % optimum.fun)
    print("status:  %s" % optimum.status)
    print("message: %s" % optimum.message)
    print("success: %s" % optimum.success)
    if optimum.success:
        print(optimum.x)

    # print("Final theta:")
    # final_theta = optimum.x.reshape(theta.shape)
    # print(final_theta)
    # plot_data(X, Y, final_theta, plot_decision=True)