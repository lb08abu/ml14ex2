#!/bin/env python3

import numpy as np


def map_features(X1, X2, depth):
    """
    Creates an array of features quadratic to each
    other. Also returns with a columns of ones in
    the first columns.
    """
    m = len(X1)
    out = np.ones((m, 1))

    for i in range(1, depth + 1):
        for j in range(i + 1):
            Xnew = (X1**(i-j) * X2**j)
            out = np.column_stack((out, Xnew))

    return out