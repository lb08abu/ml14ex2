#!/bin/python3

import numpy as np


def feature_normalise(X):
    avg = sum(X) / len(X)
    std = X.std()

    return np.divide(np.subtract(X, avg), std), avg, std
