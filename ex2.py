#!/bin/env python3

import numpy as np
import pylab as pl
from sys import exit
from sigmoid import sigmoid
from plotData import plot_data
from predict import predict
from costFunction2 import compute_cost, gradient
import scipy.optimize as op


# load data
data = np.loadtxt("ex2data1.txt", delimiter=',')
X1 = data[:, 0]
X2 = data[:, 1]
Y = data[:, 2]
m = len(Y)
Y = Y.reshape((m, 1))
X = np.column_stack((np.ones((m, 1)), X1, X2))
n = len(X[0, :])
theta = np.zeros((n, 1))

# print(X.shape)
# print(Y.shape)
# print(theta.shape)
# print(theta[0])

# plot_data(X, Y, theta)

# theta and y must be flattened for optimize!!
# this is now done in the argument passing

# compute cost
J, grad = compute_cost(theta, X, Y)

print("Initial cost function:"),
print(J)
print("Initial gradients:"),
print(grad)

if 1:
    print("Optimize:")
    optimum = op.minimize(
        compute_cost, theta.flatten(),
        method='TNC',
        jac=True, args=(X, Y.flatten()),
        options={'maxiter': 400})

    print(optimum)

    print("Final theta:")
    final_theta = optimum.x.reshape(theta.shape)
    print(final_theta)

    plot_data(X, Y, final_theta, plot_decision=True)
else:
    final_theta = np.array(
        [[-25.87350919],
         [  0.21193644],
         [  0.20722548]]
    )

# making predictions (remember we use h_theta(x))
# where h_theta(x) = 1 / 1 + e^(theta.T x)
print("Chance of student being admitted:")
exam1 = 45
exam2 = 85
print("Student with results exam1: %s, exam2: %s" % (exam1, exam2))
Xnew = np.array([1, exam1, exam2])
probability = sigmoid(final_theta.T.dot(Xnew))
print("Probability: %s" % probability)

print("Full prediction set:")
print(np.column_stack((Y, predict(X, final_theta),
                       X[:, 1], X[:, 2])))