#!/bin/env python3

import numpy as np
from sigmoid import sigmoid


def gradient(theta, x, y):
    m, n = x.shape
    hx = sigmoid(x.dot(theta.reshape(n, 1)))

    grad = (hx - y).transpose().dot(x)

    # we have to return this flattened for scipy.optimize
    grad = grad.flatten()

    # print(grad.shape)

    return grad / m


def compute_cost(theta, x, y, lamb=1):
    m, n = x.shape

    y = y.reshape((m, 1))
    ones = np.ones((m, 1))
    theta = theta.reshape((n, 1))

    # calculate hx in vectorized form (z = X.dot(theta))
    hx = sigmoid(x.dot(theta))

    # first part of the cost function
    a = (-1 * y) * ln(hx)

    # second part
    b = ((ones - y) * ln(ones - hx))

    print(a)
    print(b)

    # calculate dJ/dtheta for all thetas (vectorized):
    grad = (hx - y).T.dot(x).reshape((n, 1)) / m
    grad += np.vstack((np.array([[1]]), (theta[1:] * (lamb / m))))

    # calculate J
    reg = lamb / (2*m) * theta[1:].T.dot(theta[1:])
    J = ((np.sum(a - b) + reg[0, 0]) / m)

    # print(grad)
    # print(J)

    return J, grad


def f(x):
    """
    Returns log(x) if x >= 0
    Otherwise handles accordingly
    """
    if x >= 0:
        return np.log(x)
    elif x == 0:
        return -9999
    else:
        return 0


def ln(M):
    """
    Array form of f(x). Performs log on all elements
    and returns 0 if element is negative.
    """
    return np.array([f(a) for a in M.flat]).reshape((M.shape))
