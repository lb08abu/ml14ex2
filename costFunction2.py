#!/bin/env python3

import numpy as np
from sigmoid import sigmoid


def gradient(theta, x, y):
    m, n = x.shape
    hx = sigmoid(x.dot(theta.reshape(n, 1)))

    grad = (hx - y).transpose().dot(x)

    # we have to return this flattened for scipy.optimize
    grad = grad.flatten()

    # print(grad.shape)

    return grad / m


def compute_cost(theta, x, y):
    m, n = x.shape

    y = y.reshape(m, 1)
    ones = np.ones((m, 1))
    theta = theta.reshape((n, 1))
    # calculate z in vectorized form (= X.dot(theta))
    z = x.dot(theta)

    # calculate hx
    hx = sigmoid(z)

    # first part of the cost function
    a = (-1 * y) * ln(hx)

    # second part
    b = ((ones - y) * ln(ones - hx))

    # calculate dJ/dtheta for all thetas:
    # grad = (hx - y).T.dot(x).reshape(theta.shape) / m
    grad = (hx - y).T.dot(x).flatten() / m

    # calculate J
    J = (np.sum(a - b) / m)

    # print(grad)
    # print(J)

    return J, grad


def f(x):
    """
    Returns log(x) if x >= 0
    Otherwise handles accordingly
    """
    if x >= 0:
        return np.log(x)
    elif x == 0:
        return -9999
    else:
        return 0


ln = np.vectorize(f)
