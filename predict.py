#!/bin/env python3

from sigmoid import sigmoid
import numpy as np


def predict(X, theta):
    """
    Predicts if a student will be admitted.
    Takes an array of exam scores and returns
    an array of 0's (for no) and 1's (for yes).
    """

    m, n = X.shape
    A = np.zeros((m, 1))

    for i in range(m):
        A[i] = sigmoid(theta.T.dot(X[i, :]))

        if A[i] >= 0.5:
            A[i] = 1
        else:
            A[i] = 0

    return A