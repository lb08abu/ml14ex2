#!/bin/env python3

import numpy as np
import math
from sigmoid import sigmoid


def compute_cost(x, y, theta, m):

    J = 0
    for i in range(m):
        z = theta.transpose().dot(x[i])

        J += (-y[i] * math.log(sigmoid(z)) -
             (1 - y[i]) * math.log(1 - sigmoid(z)))

    J = float(J) / m

    return J


def dJ(x, y, theta):
    a = np.add(y * np.log(s),
               (np.subtract(1, y)) * np.log(np.subtract(1, s)))
    return a

