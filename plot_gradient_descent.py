"""
Plots the gradient descent results.
"""

import pylab as pl


def plot_results(J_history):

    pl.figure()
    pl.subplot(221)
    pl.plot(J_history[:, 0], J_history[:, 29])
    pl.title("J history")
    
    pl.subplot(222)
    pl.plot(J_history[:, 0], J_history[:, 1])
    pl.title("theta 0")
    
    pl.subplot(223)
    pl.plot(J_history[:, 0], J_history[:, 2])
    pl.title("theta 1")
    
    pl.subplot(224)
    pl.plot(J_history[:, 0], J_history[:, 10])
    pl.title("theta 10")

    pl.show()