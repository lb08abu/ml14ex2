#!/bin/env python3

import numpy as np
import pylab as pl


def plot_data(X, Y, theta, plot_decision=False):

    # create arrays of X via list comprehension across Y
    A = np.array([X[i, 1:] for i, j in enumerate(Y[:, 0]) if int(j) == 1])
    B = np.array([X[i, 1:] for i, j in enumerate(Y[:, 0]) if int(j) == 0])

    pl.figure()
    pl.subplot(111)
    pl.xlim(25, 100)
    pl.ylim(25, 100)
    Ap = pl.scatter(A[:, 0], A[:, 1], marker='x', c='black')
    Bp = pl.scatter(B[:, 0], B[:, 1], marker='o', c='yellow')
    pl.xlabel("Exam 1 score")
    pl.ylabel("Exam 2 score")
    pl.title("University Admissions")

    if plot_decision:
        pass
        plot_x = [min(X[:, 2])-6, max(X[:, 2])+2]
        plot_y = (-1 / theta[2]) * (theta[1] * plot_x + theta[0])

        pl.plot(plot_x, plot_y, label="Decision Boundary")


    pl.legend((Ap, Bp), ("Admitted", "Not Admitted"),
              loc='lower right', fontsize='10')

    pl.show()


def plot_data2(X, Y):
    A = np.array([X[i, 1:] for i, j in enumerate(Y[:, 0]) if int(j) == 1])
    B = np.array([X[i, 1:] for i, j in enumerate(Y[:, 0]) if int(j) == 0])

    # gets the max values for the plot range
    xmax = max(abs(X[:, 1])) * 1.05
    ymax = max(abs(X[:, 2])) * 1.05

    pl.figure()
    pl.subplot(111)
    pl.xlim(-xmax, xmax)
    pl.ylim(-ymax, ymax)
    Ap = pl.scatter(A[:, 0], A[:, 1], marker='x', c='black')
    Bp = pl.scatter(B[:, 0], B[:, 1], marker='o', c='yellow')
    pl.xlabel("Exam 1 score")
    pl.ylabel("Exam 2 score")
    pl.title("Microchip QC")

    pl.legend((Ap, Bp), ("Passed", "Failed"),
              loc='lower right', fontsize='10')

    pl.show()